## 说明

> 项目中引入请使用<b>release分支</b>，下面为具体分支的不同作用，请根据自己需要选择。

**分支区别：**

> - main：主分支，会定期合并最新代码
> - master：主分支，包含演示代码(学习建议拉取该分支代码)
> - release：发布分支，只包含核心代码，不包含演示代码(<b>项目引入推荐使用该分支</b>)

## 项目引入流程

&emsp;&emsp;**1、从Gitee或者Github拉去release分支最新代码到本地**

![img](https://it-diary-1308244209.cos.ap-guangzhou.myqcloud.com//image20220910183557.png)

&emsp;&emsp;**2、在IDEA中引入项目，并打包到本地仓库**


![image-20220910183805127](https://it-diary-1308244209.cos.ap-guangzhou.myqcloud.com//image20220910183805.png)


&emsp;&emsp;**3、在新项目中引入依赖**


&emsp;&emsp;说明：注意添加exclusion排除掉jackson依赖是因为seaweedfs下这个依赖回合项目中的springboot-starter中的存在冲突，不排除的话springboot程序启动会出现：ClassNotFoundException: com.fasterxml.jackson.core.util.JacksonFeature

```java
<dependency>
    <groupId>cn.it.learning</groupId>
    <artifactId>it-wheels-emp-imp-util</artifactId>
    <version>1.0-SNAPSHOT</version>
    <exclusions>
        <exclusion>
        <artifactId>jackson-core</artifactId>
        <groupId>com.fasterxml.jackson.core</groupId>
        </exclusion>
    </exclusions>
</dependency>

```

&emsp;&emsp;**4、在启动类中添加组件扫描路径** (因为工具类中有注入到Spring容器的，如果不扫描，使用自动装配时就会出现空指针异常，请注意：扫描路径记得同样要记得添加你自己项目的路径，不然你的实体bean也会注入不到容器中)


```java
@ComponentScan(basePackages = {"cn.it.learning","it.test"})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }
}
```

![image-20220910185220660](https://it-diary-1308244209.cos.ap-guangzhou.myqcloud.com//image20220910185220.png)


## 写在最后

&emsp;&emsp;如果按照上面步骤引入后程序启动失败的，建议首先排查pom.xml文件中是否有依赖冲突了(个人在整理时发现这个问题比较容易出现)，排查的时候可以借助IDEA中的maven helper插件，有冲突直接排除掉就行。

&emsp;&emsp;如果经过个人排查无法解决的，请给项目提issue，博主看到后会尽快处理的。**如果项目对您有帮助，不要忘记给个Star和推荐给需要的朋友哦。**