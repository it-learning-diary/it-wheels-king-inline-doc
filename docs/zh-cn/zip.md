## 使用示例

&emsp;&emsp;说明：需要先将项目打包成jar，然后在pom文件中引入。

&emsp;&emsp;ZIP工具简介：在一些报表/政府系统中，一个页面通常有多个报表需要生成，使用压缩包统一导出的方式非常适合，本工具提供了打包指定文件和指定路径下的所有文件两种API，方便大家压缩导出。

### 1、引入依赖

```java
<dependency>
    <groupId>cn.it.learning</groupId>
    <artifactId>it-wheels-emp-imp-util</artifactId>
    <version>1.0-SNAPSHOT</version>
    <exclusions>
        <exclusion>
        <artifactId>jackson-core</artifactId>
        <groupId>com.fasterxml.jackson.core</groupId>
        </exclusion>
    </exclusions>
</dependency>
```

### 2、使用案例

```java

 /***
    * 压缩给定文件并导出
    */
    @PostMapping("/zip/export_confirm_files")
    public void compressConfirmFilesToZip(HttpServletResponse response,String file) throws Exception {
        ZipUtil.exportZipByFiles(response, "demohello.zip", new File(file));
    }

    /***
    * 压缩给定路径下的所有文件到zip并导出
    */
    @PostMapping("/zip/export_filepath_files")
    public void compressFilePathAllFileToZip(HttpServletResponse response,String filePath) throws Exception {
        ZipUtil.exportZipFileByFilePath(response,filePath,"demo2222.zip");
    }


```