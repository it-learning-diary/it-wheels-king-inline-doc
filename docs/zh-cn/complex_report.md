## 使用示例

&emsp;&emsp;**说明：** 需要先将项目打包成jar，然后在pom文件中引入。

&emsp;&emsp;**工具简介：** 复杂报表在一些统计系统或者政府项目中使用的非常频繁，许多时候他们的表头和数据都是变动的，此时使用想poi或者easyexcel通过程序控制就会比较繁琐，而且可读性和可维护性不高。

&emsp;&emsp;**此时，我们则可以引入模板引擎，通过占位符+模板+数据的方式，完成复杂报表的导出，freemaker则是非常流行的一种模板引擎。**

### 1、引入依赖

```java
<dependency>
    <groupId>cn.it.learning</groupId>
    <artifactId>it-wheels-emp-imp-util</artifactId>
    <version>1.0-SNAPSHOT</version>
    <exclusions>
        <exclusion>
        <artifactId>jackson-core</artifactId>
        <groupId>com.fasterxml.jackson.core</groupId>
        </exclusion>
    </exclusions>
</dependency>
```

### 2、编写模板(以excel模板示例，word文档其实也类似)

&emsp;&emsp;(1)、在电脑创建xlsx或者xls后缀的文件，并使用office或者wps打开

![image-20220910191455709](https://it-diary-1308244209.cos.ap-guangzhou.myqcloud.com//image20220910191455.png)

&emsp;&emsp;(2)、根据实际业务编写号占位符，并另存为.xml文件

![excel模板另存为](https://it-diary-1308244209.cos.ap-guangzhou.myqcloud.com//image20220910191614.png)

&emsp;&emsp;word模板另存为：

![word模板另存为](https://it-diary-1308244209.cos.ap-guangzhou.myqcloud.com//image20220910192242.png)

&emsp;&emsp;(3)、将xml文件名修改为ftl文件名，并模板添加到项目中，一般是放在resources下的template目录下，然后调用工具的API填充数据即可完成导出

![image-20220910191743082](https://it-diary-1308244209.cos.ap-guangzhou.myqcloud.com//image20220910191743.png)

## 特别说明

&emsp;&emsp;如果模板是word文档的话，先使用文字占位，然后在修改为xml文件后使用notepad等工具再将问题替换成对应的${xxx}占位符，否则的话，word在另存为xml时可能会把${}中间填充其他东西，这样模板解析会存在问题(亲测有坑)。

&emsp;&emsp;**推荐阅读：**在模板中需要涉及到一些逻辑的，需要结合freemarker语法，推荐阅读官方文档：[FreeMarker使用手册](http://www.kerneler.com/freemarker2.3.23/)

